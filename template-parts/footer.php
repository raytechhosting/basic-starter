<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.2.0
 * @version 0.2.0
 */

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

$raytech_basic_starter_config = get_option( THEME_NAME . '_theme_options' );
?>
<footer>
	<?php if ( 0 !== $raytech_basic_starter_config['footer_columns'] ) : ?>
		<div class="grid grid-cols-<?php echo esc_attr( $raytech_basic_starter_config['footer_columns'] ); ?>">
			<?php
			for ( $raytech_basic_starter_footer = 1; $raytech_basic_starter_footer <= $raytech_basic_starter_config['footer_columns']; $raytech_basic_starter_footer++ ) {
				echo '<div id="footer-' . esc_attr( $raytech_basic_starter_footer ) . '">';
				dynamic_sidebar( 'footer-' . $raytech_basic_starter_footer );
				echo '</div>';
			}
			?>
		</div>
	<?php endif; ?>
</footer>
