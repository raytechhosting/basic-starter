<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.1.0
 * @version 0.1.0
 */

namespace RayTech\BasicStarter\Includes;

use RayTech\BasicStarter\Includes\Customizer;
use RayTech\BasicStarter\Includes\Sidebars;
use RayTech\BasicStarter\Includes\Styles;
use RayTech\BasicStarter\PostTypes\Staff;
use RayTech\BasicStarter\Elementor\Extension;

/**
 * Functions class for all the functions in the theme.
 */
class Functions {

	/**
	 * This is the entry point of all functions for the theme
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'setup'] );
		add_action( 'elementor/theme/register_locations', [$this, 'register_elementor_locations'] );
		new Styles();
		new Sidebars();
		new Customizer();
		new Extension();
	}

	/**
	 * Register elementor locations
	 *
	 * @param  mixed $elementor_theme_manager Elementor theme manager object.
	 * @return void
	 */
	public function register_elementor_locations( $elementor_theme_manager ) {

		$elementor_theme_manager->register_all_core_location();

	}

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	public function setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on evoluciel, use a find and replace
		 * to change 'evoluciel' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'basicstarter', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'yoast-seo-breadcrumbs' );

	}
}
