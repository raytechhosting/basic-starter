<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.1.0
 * @version 0.1.0
 */

namespace RayTech\BasicStarter\Includes;

/**
 * Customizer configuration class
 */
class Customizer {

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'customize_register', [$this, 'customize_register'] );
	}

	/**
	 * Customize registering method
	 *
	 * @param  mixed $wp_customize Customizer object.
	 * @return void
	 */
	public function customize_register( $wp_customize ) {
		$wp_customize->add_section(
			THEME_NAME . '_theme_options2',
			[
				'title'       => __( 'Theme Options', 'basicstarter' ),
				'description' => 'You can switch out some options for the theme in here.',
				'priority'    => 120,
			]
		);

		$wp_customize->add_setting(
			THEME_NAME . '_theme_options[footer_columns]',
			[
				'default'    => 4,
				'capability' => 'edit_theme_options',
				'type'       => 'option',
			]
		);

		$wp_customize->add_control(
			THEME_NAME . '_footer_columns',
			[
				'label'       => __( 'Footer Columns', 'basicstarter' ),
				'section'     => THEME_NAME . '_theme_options2',
				'description' => 'This will change the nuber of widgets available.',
				'settings'    => THEME_NAME . '_theme_options[footer_columns]',
				'type'        => 'number',
				'input_attrs' => [
					'max' => '4',
					'min' => '0',
				],
			]
		);
	}
}
