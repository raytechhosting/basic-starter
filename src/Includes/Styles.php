<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.1.0
 * @version 0.2.3
 */

namespace RayTech\BasicStarter\Includes;

/**
 * Styles class to enqueue styles and javascript to WordPress
 */
class Styles {

	/**
	 * Constructor Methods
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'scripts'] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_scripts'] );
	}

	/**
	 * Enqueue scripts and styles.
	 */
	public function scripts() {
		wp_enqueue_style( 'basic-starter-main-style', get_stylesheet_directory_uri() . '/assets/dist/frontend.css', [], RAYTECH_BASIC_STARTER_VERSION, 'all' );
		wp_enqueue_script( 'basic-starter-script', get_stylesheet_directory_uri() . '/assets/dist/frontend.js', [], RAYTECH_BASIC_STARTER_VERSION, true );
		wp_style_add_data( 'basic-starter-style', 'rtl', 'replace' );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	/**
	 * Enqueue administrative scripts and styles.
	 */
	public function admin_scripts() {
		wp_enqueue_style( 'basic-starter-admin-style', get_template_directory_uri() . '/assets/dist/admin.css', [], RAYTECH_BASIC_STARTER_VERSION );
		wp_enqueue_script( 'basic-starter-admin', get_template_directory_uri() . '/assets/dist/admin.js', ['wp-util'], RAYTECH_BASIC_STARTER_VERSION, true );
	}

}
