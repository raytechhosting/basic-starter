<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.1.0
 * @version 0.1.0
 */

namespace RayTech\BasicStarter\Includes;

/**
 * Sidebars setup classes
 */
class Sidebars {

	/**
	 * Configuration array from wp customizer.
	 *
	 * @var array $config
	 */
	protected $config;

	/**
	 * Constructor method.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->config = get_option( THEME_NAME . '_theme_options' );
		add_action( 'after_setup_theme', [ $this, 'register_sidebars'] );
	}

	/**
	 * Register sidebars method
	 *
	 * @return void
	 */
	public function register_sidebars() {
		// Aside sidebar.
		register_sidebar(
			[
				'name'          => esc_html__( 'Sidebar', 'basicstarter' ),
				'id'            => 'sidebar-1',
				'description'   => esc_html__( 'Add widgets here.', 'basicstarter' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]
		);

		if ( (int) $this->config['footer_columns'] >= 1 ) {
			register_sidebar(
				[
					'name'          => esc_html__( 'Footer', 'basicstarter' ) . ' 1',
					'id'            => 'footer-1',
					'description'   => esc_html__( 'Add widgets here.', 'basicstarter' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				]
			);
		}
		if ( $this->config['footer_columns'] >= 2 ) {
			register_sidebar(
				[
					'name'          => esc_html__( 'Footer', 'basicstarter' ) . ' 2',
					'id'            => 'footer-2',
					'description'   => esc_html__( 'Add widgets here.', 'basicstarter' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				]
			);
		}
		if ( $this->config['footer_columns'] >= 3 ) {
			register_sidebar(
				[
					'name'          => esc_html__( 'Footer', 'basicstarter' ) . ' 3',
					'id'            => 'footer-3',
					'description'   => esc_html__( 'Add widgets here.', 'basicstarter' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				]
			);
		}
		if ( $this->config['footer_columns'] >= 4 ) {
			register_sidebar(
				[
					'name'          => esc_html__( 'Footer', 'basicstarter' ) . ' 4',
					'id'            => 'footer-4',
					'description'   => esc_html__( 'Add widgets here.', 'basicstarter' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				]
			);
		}
	}
}
