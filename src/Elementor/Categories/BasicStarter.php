<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.2.0
 * @version 0.2.0
 */

namespace RayTech\BasicStarter\Elementor\Categories;

/**
 * Basic Starter widget category class
 */
class BasicStarter {
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'elementor/elements/categories_registered', [ $this, 'add_categories' ] );
	}

	/**
	 * Add category to the Elementor widget section.
	 *
	 * @param  mixed $elements_manager Elementor Category elements manager.
	 * @return void
	 */
	public function add_categories( $elements_manager ) {
		$elements_manager->add_category(
			'basic_starter',
			[
				'title' => __( 'Basic Starter', 'basicstarter' ),
				'icon'  => 'fa fa-plug',
			]
		);
	}
}
