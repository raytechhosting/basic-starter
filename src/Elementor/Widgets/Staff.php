<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.2.0
 * @version 0.2.0
 */

namespace RayTech\BasicStarter\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Icons_Manager;
use Elementor\Widget_Base;

/**
 * Staff widget class for elementor
 */
class Staff extends Widget_Base {

	/**
	 * Getter function to get widget name.
	 *
	 * @return string
	 */
	public function get_name() {
		return 'Staff';
	}

	/**
	 * Getter function to get widget title.
	 *
	 * @return string
	 */
	public function get_title() {
		return __( 'Staff', 'basicstarter' );
	}

	/**
	 * Getter function to get icon.
	 *
	 * @return string
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	/**
	 * Function to decide in which group of widgets this one shows in
	 *
	 * @return array
	 */
	public function get_categories() {
		return [ 'basic_starter' ];
	}

	/**
	 * Register widget controls.
	 *
	 * @return void
	 */
	public function register_controls() {
		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'basicstarter' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'picture',
			[
				'label' => esc_html__( 'Picture', 'basicstarter' ),
				'type'  => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'name',
			[
				'label' => esc_html__( 'Name', 'basicstarter' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'basicstarter' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$this->add_control(
			'text',
			[
				'label' => esc_html__( 'Text', 'basicstarter' ),
				'type'  => Controls_Manager::TEXTAREA,
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'social_icon',
			[
				'label' => esc_html__( 'Icon', 'basicstarter' ),
				'type'  => Controls_Manager::ICONS,
			]
		);

		$repeater->add_control(
			'link',
			[
				'label'   => esc_html__( 'Link', 'basicstarter' ),
				'type'    => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
			]
		);
		$repeater->add_control(
			'text',
			[
				'label' => esc_html__( 'Text', 'basicstarter' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$repeater->add_control(
			'color',
			[
				'label'     => esc_html__( 'Color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  {{CURRENT_ITEM}} a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'social',
			[
				'label'  => esc_html__( 'Social links', 'basicstarter' ),
				'type'   => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Style Section', 'basicstarter' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_position',
			[
				'label'   => esc_html__( 'Picture position', 'basicstarter' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left'  => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'default' => 'left',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Image_Size::get_type(),
			[
				'name'      => 'picture',
				'default'   => 'large',
				'separator' => 'none',
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'basicstarter' ),
			]
		);

		$this->add_control(
			'global-heading',
			[
				'label'     => esc_html__( 'Global', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'global-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_TEXT,
				],
			]
		);

		$this->add_control(
			'global-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'global-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'default'   => 'left',
				'selectors' => [
					'{{WRAPPER}} .staff-info' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'name-heading',
			[
				'label'     => esc_html__( 'Name', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'name-typography',
				'selector' => '{{WRAPPER}} .staff-name',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_PRIMARY,
				],
			]
		);

		$this->add_control(
			'name-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-name' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'name-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-name' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title-heading',
			[
				'label'     => esc_html__( 'Title', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'title-typography',
				'selector' => '{{WRAPPER}} .staff-title',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_SECONDARY,
				],
			]
		);

		$this->add_control(
			'title-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-title' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'text-heading',
			[
				'label'     => esc_html__( 'Text', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'text-typography',
				'selector' => '{{WRAPPER}} .staff-text',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_SECONDARY,
				],
			]
		);

		$this->add_control(
			'text-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-text' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'text-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-text' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'social-heading',
			[
				'label'     => esc_html__( 'Social', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'social-typography',
				'selector' => '{{WRAPPER}} .staff-social',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_TEXT,
				],
			]
		);

		$this->add_control(
			'social-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-social a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'social-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-social' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'basicstarter' ),
			]
		);

		$this->add_control(
			'global-hover-heading',
			[
				'label'     => esc_html__( 'Global', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'global-hover-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper:hover',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_TEXT,
				],
			]
		);

		$this->add_control(
			'global-hover-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'global-hover-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'default'   => 'left',
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'name-hover-heading',
			[
				'label'     => esc_html__( 'Name', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'name-hover-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper:hover .staff-name',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_PRIMARY,
				],
			]
		);

		$this->add_control(
			'name-hover-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-name' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'name-hover-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-name' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title-hover-heading',
			[
				'label'     => esc_html__( 'Title', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'title-hover-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper:hover .staff-title',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_SECONDARY,
				],
			]
		);

		$this->add_control(
			'title-hover-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title-hover-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-title' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'text-hover-heading',
			[
				'label'     => esc_html__( 'Text', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'text-hover-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper:hover .staff-text',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_SECONDARY,
				],
			]
		);

		$this->add_control(
			'text-hover-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-text' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'text-hover-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-text' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'social-hover-heading',
			[
				'label'     => esc_html__( 'Social', 'basicstarter' ),
				'type'      => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name'     => 'social-hover-typography',
				'selector' => '{{WRAPPER}} .staff-wrapper:hover .staff-social',
				'label'    => esc_html__( 'Typography', 'basicstarter' ),
				'global'   => [
					'default' => \Elementor\Core\Kits\Documents\Tabs\Global_Typography::TYPOGRAPHY_TEXT,
				],
			]
		);

		$this->add_control(
			'social-hover-color',
			[
				'label'     => esc_html__( 'Text color', 'basicstarter' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-social a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'social-hover-alignment',
			[
				'type'      => \Elementor\Controls_Manager::CHOOSE,
				'label'     => esc_html__( 'Alignment', 'basicstarter' ),
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'basicstarter' ),
						'icon'  => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'basicstarter' ),
						'icon'  => 'eicon-text-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'basicstarter' ),
						'icon'  => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .staff-wrapper:hover .staff-social ' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Rendering function.
	 *
	 * @return void
	 */
	public function render() {
		$settings     = $this->get_settings_for_display();
		$allowed_html = [
			'img' => [
				'src'     => [],
				'alt'     => [],
				'title'   => [],
				'class'   => [],
				'loading' => [],
			],
		];
		?>
		<style>
			.staff-wrapper {
				display: grid;
				<?php
				if ( 'right' === $settings['img_position'] ) {
					echo 'grid-template-columns: 1fr minmax(0, 200px);';
				} else {
					echo 'grid-template-columns: minmax(0, 200px) 1fr;';
				}
				?>
			}
			.staff-img,
			.staff-info,
			.icon-wrapper {
				display: inline-block;
			}
		</style>
		<div class="staff-wrapper">
			<?php if ( 'left' === $settings['img_position'] ) : ?>
			<div class="staff-img">
				<?php
				echo wp_kses( Group_Control_Image_Size::get_attachment_image_html( $settings, 'picture', 'picture' ), $allowed_html );
				?>
			</div>
			<?php endif; ?>
			<div class="staff-info">
				<div class="staff-name"><?php echo esc_html( $settings['name'] ); ?></div>
				<div class="staff-title"><?php echo esc_html( $settings['title'] ); ?></div>
				<div class="staff-text"><?php echo esc_html( $settings['text'] ); ?></div>
				<div class="staff-social">
				<?php
				if ( ! empty( $settings['social'] ) ) {
					foreach ( $settings['social'] as $social ) {
						?>
					<div class="icon-wrapper elementor-repeater-item-<?php echo esc_attr( $social['_id'] ); ?>" >
						<a href="
						<?php
						echo esc_url( $social['link']['url'] );
						?>
						">
						<?php
						Icons_Manager::render_icon( $social['social_icon'], [ 'aria-hidden' => 'true' ] );
						if ( '' !== $social['text'] ) {
							echo esc_html( $social['text'] );
						}
						?>
						</a>
					</div>
						<?php
					}
				}
				?>
				</div>
			</div>
			<?php if ( 'right' === $settings['img_position'] ) : ?>
			<div class="staff-img">
				<?php
				echo wp_kses( Group_Control_Image_Size::get_attachment_image_html( $settings, 'picture', 'picture' ), $allowed_html );
				?>
			</div>
			<?php endif; ?>
		</div>
		<?php
	}
}
