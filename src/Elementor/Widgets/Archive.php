<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.2.0
 * @version 0.2.0
 */

namespace RayTech\BasicStarter\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use WP_Query;

/**
 * Archive elementor widget class
 */
class Archive extends Widget_Base {

	/**
	 * Getter function to get widget name.
	 *
	 * @return string
	 */
	public function get_name() {
		return 'PostArchive';
	}

	/**
	 * Getter function to get widget title.
	 *
	 * @return string
	 */
	public function get_title() {
		return __( 'Post Archive ', 'basicstarter' );
	}

	/**
	 * Getter function to get icon.
	 *
	 * @return string
	 */
	public function get_icon() {
		return 'eicon-archive-posts';
	}

	/**
	 * Function to decide in which group of widgets this one shows in
	 *
	 * @return array
	 */
	public function get_categories() {
		return ['basic_starter'];
	}

	/**
	 * Register controls
	 *
	 * @return void
	 */
	protected function register_controls() {
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'basicstarter' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'columns',
			[
				'label'   => __( 'Columns', 'basicstarter' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '3',
				'options' => [
					1 => '1',
					2 => '2',
					3 => '3',
					4 => '4',
					5 => '5',
					6 => '6',
				],
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label'   => __( 'Posts per page', 'basicstarter' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => '-1',
				'min'     => '-1',
			]
		);

		$this->add_control(
			'post_type',
			[
				'label'   => __( 'Post Type', 'basicstarter' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post',
				'options' => [
					'post' => __( 'Posts', 'basicstarter' ),
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Query method
	 *
	 * @param  mixed $type Post type.
	 * @param  mixed $limit NUmber of posts.
	 * @return WP_Query
	 */
	protected function query( $type = 'post', $limit = -1 ) {
		global $wp_query;

		$args = [
			'post_type'      => $type,
			'posts_per_page' => $limit,
		];

		if ( $wp_query->query['s'] ) {
			$args['s'] = urldecode( $wp_query->query['s'] );
		}

		return new WP_Query( $args );
	}

	/**
	 * Rendering method
	 *
	 * @return void
	 */
	public function render() {
		global $wp_query;
		$settings = $this->get_settings_for_display();
		$query    = $this->query( $settings['post_type'], $settings['posts_per_page'] );

		if ( $wp_query->query['s'] && ! $query->have_posts() ) {
			echo esc_html__( 'No results were found', 'basicstarter' );
		} else {
			echo '<div class="grid grid-cols-1 gap-2">';
			foreach ( $query->get_posts() as $post ) {
				echo '<div>'
				. '<a class="font-bold text-xs" href="' . esc_url( get_permalink( $post ) ) . '"><span class="text-primary font-bold text-lg font-content">' . esc_html( $post->post_title ) . '</span></a>'
				. '<br>';
				if ( empty( $post->post_excerpt ) ) {
					echo esc_html( $post->description );
				} else {
					echo esc_html( $post->post_excerpt );
				}
				echo '</div>';
			}
			echo '</div>';
		}
	}
}
