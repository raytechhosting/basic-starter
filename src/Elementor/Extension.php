<?php
/**
 * Copyright (C) 2020 RayTech Hosting <royk@myraytech.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Kevin Roy <royk@myraytech.net>
 * @package WordPress
 * @subpackage Basic Starter
 * @since 0.2.0
 * @version 0.2.0
 */

namespace RayTech\BasicStarter\Elementor;

use Elementor\Plugin;
use RayTech\BasicStarter\Elementor\Widgets;
use RayTech\BasicStarter\Elementor\Categories\BasicStarter;

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

define( 'RAYTECH_BASIC_STARTER_ELEMENTOR', __FILE__ );

/**
 * Elementor Extension for adding widgets and and more to elementor
 */
final class Extension {


	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '3.6.4';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.4';

	/**
	 * Constructor method to add the warnings to the admin panel.
	 *
	 * @return void
	 */
	public function __construct() {
		// Check if Elementor installed and activated.
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [$this, 'admin_notice_missing_main_plugin'] );
			return;
		}

		// Check for required Elementor version.
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [$this, 'admin_notice_minimum_elementor_version'] );
			return;
		}

		// Check for required PHP version.
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [$this, 'admin_notice_minimum_php_version'] );
			return;
		}

		new BasicStarter();
		// Register widgets.
		add_action( 'elementor/widgets/widgets_registered', [$this, 'register_widgets'] );
	}

	/**
	 * Register widgets method
	 *
	 * @return void
	 */
	public function register_widgets() {
		Plugin::instance()->widgets_manager->register( new Widgets\Staff() );
	}

	/**
	 * Main elementor plugin admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {
		deactivate_plugins( plugin_basename( RAYTECH_BASIC_STARTER_ELEMENTOR ) );

		return sprintf(
			wp_kses(
				'<div class="notice notice-warning is-dismissible"><p><strong>"%1$s"</strong> requires <strong>"%2$s"</strong> to be installed and activated.</p></div>',
				[
					'div' => [
						'class'  => [],
						'p'      => [],
						'strong' => [],
					],
				]
			),
			'Elementor Basic Starter',
			'Elementor'
		);
	}

	/**
	 * Minimum Elementor version admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {
		deactivate_plugins( plugin_basename( RAYTECH_BASIC_STARTER_ELEMENTOR ) );

		return sprintf(
			wp_kses(
				'<div class="notice notice-warning is-dismissible"><p><strong>"%1$s"</strong> requires <strong>"%2$s"</strong> version %3$s or greater.</p></div>',
				[
					'div' => [
						'class'  => [],
						'p'      => [],
						'strong' => [],
					],
				]
			),
			'Elementor Basic Starter',
			'Elementor',
			self::MINIMUM_ELEMENTOR_VERSION
		);
	}

	/**
	 * Minimum PHP version admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {
		deactivate_plugins( plugin_basename( RAYTECH_BASIC_STARTER_ELEMENTOR ) );

		return sprintf(
			wp_kses(
				'<div class="notice notice-warning is-dismissible"><p><strong>"%1$s"</strong> requires <strong>"%2$s"</strong> version %3$s or greater.</p></div>',
				[
					'div' => [
						'class'  => [],
						'p'      => [],
						'strong' => [],
					],
				]
			),
			'Elementor Basic Starter',
			'Elementor',
			self::MINIMUM_ELEMENTOR_VERSION
		);
	}

}
