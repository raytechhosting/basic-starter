# Change Log

All notable changes to this project will be documented in this file.

## [0.2.0] - yyyy-mm-dd

Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.

### Added

- Added basic template files for use with Elementor Pro function "elementor_theme_do_location".
- All input for metaboxes are now classes extending the abstract input class.
- Added Elementor widget for building staff page.
- Added example files for full custom post type of staffs.

### Changed

- Moved all template files in the template-parts folder.
- Changed Namespace from Basic/Starter to RayTech/BasicStarter of package.
- You can now create all meta boxes to a simple array configuration.
- Moved JS, CSS to child theme as it is not required for this theme.

### Fixed

## [0.1.0] - 2020-03-15

This was the initial setup of the theme project.

### Added

- Added starter theme files for WordPress called "_s".
- Added Abstract classes for Permalink, Post type, Taxonomies(tag and category).
- Added Composer for theme dependencies and PSR-4 autoloading of classes.

### Changed
  
- Changed all instances of string _s to basicstarter.
- Modify all functional code to Object-Oriented Programming.
