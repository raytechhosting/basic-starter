# Basic Starter WP Theme

This is a starting point to create a theme from scratch.

## Installation

 composer install
 npm install

Then you need to upload to server

## Details

This theme utilizes normalize thru npm and webpack bundler ready , and TGM Plugin Activation thru composer as dependencies.

This theme has multiple abstract PHP classes easy to use and create:

- Post Types
- Taxonomies relating to post types
- Permalinks options for post types
- Meta boxes related to post types
